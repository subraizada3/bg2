#!/usr/bin/env python3

import os, shutil

# change working directory to this script's folder
# https://stackoverflow.com/a/1432949
abspath = os.path.abspath(__file__)
dirname = os.path.dirname(abspath)
os.chdir(dirname)

def confirm(msg):
	return input(msg).lower().startswith('y')

def rm(f):
	if os.path.exists(f):
		if os.path.isdir(f): shutil.rmtree(f, ignore_errors=True)
		else: os.remove(f)



print("\nAt each prompt, enter 'y' to delete the file or 'n' to keep it\n")

rm('readme.md')

if confirm('Development files (git version history & PyCharm project: '):
	rm('.git'); rm('.idea'); rm('.gitignore')

if confirm('Sublime Text project file: '):
	rm('bg2.sublime-project')

if confirm('Kate text editor project file: '):
	rm('.kateproject')

if confirm('LICENSE.txt: '):
	rm('LICENSE.txt')

if confirm('Documentation (in staging/ and live/ directories): '):
	for dir in ['staging', 'live']:
		rm(dir); os.makedirs(dir)

if confirm('This installer: '):
	rm('install.py')
