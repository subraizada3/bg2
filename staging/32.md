2018-07-04 Code highlighting and LaTeX

{{include mathjax}}

## Code highlighting

The source code bundles [codehilite CSS from Rich Leland](http://richleland.github.io/pygments-css/), and a simple file copy/replace can be used to change the codehilite theme in use. The codehilite CSS is included by default on every page, adding 4.4kb/page. To avoid this, you can delete the <code class='codehilite'>{{include&nbsp;codehilite}}</code> line from the HTML templates (index.html and post.html) and place the include statement only inside markdown posts where code highlighting is required.

Usually, it can guess the language to use for highlighting, but you can also specify it manually - see the [documentation](https://python-markdown.github.io/extensions/code_hilite/#syntax).

```
import sys

def print_numbers():
	for i in range(10):
		print('The number is {}'.format(i*2))

if __name__ == '__main__':
	print_numbers()
	sys.exit(0)
```

It also works `:::python with` inline code blocks.

```
#!java
import java.util.ArrayList;

public class Main {

	ArrayList<Integer> ints = new ArrayList<>();

	public static void main(String[] args) {
		ints.add(192304);
		System.out.println(ints);
	}
}
```

```
#!html
<html>
<head><title>this is some html</title></head>
<body>
<p> paragraph </p>
<!-- comment -->
</body>
</html>
```

## LaTeX

LaTeX can be inserted inline like this $x^2 + y^2 = z^2$, or it can be inserted as a block [block LaTeX code from [here](https://math.meta.stackexchange.com/questions/5020/mathjax-basic-tutorial-and-quick-reference)]:

$$\sum_{i=0}^n i^2 = \frac{(n^2+n)(2n+1)}{6}$$

To render it inline, wrap the LaTeX in dollar signs: `$x^2 + y^2 = z^2$`. To make a block of math, put two <kbd>$</kbd>s on each side.

LaTeX is rendered via MathJax, by default served via the CloudFlare JS CDN, though it is trivial to edit the templates and have it served locally. By default it is not included with the HTML; just place the text <code class='codehilite'>{{include&nbsp;mathjax}}</code> inside your markdown post to include the MathJax script, or place the include statement within the template files (index.html and post.html) to have it included by default.
