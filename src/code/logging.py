# functions for printing error messages

import traceback

def error(msg: str):
	print('====================   ERROR   ====================')
	print(msg)
	print()

def trace(e: Exception, msg: str):
	error(msg)
	print('==================   TRACEBACK   ==================')
	traceback.print_exc(e)
	print('\n\n\n\n')
