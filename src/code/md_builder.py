from src.code.config import Config
from pymdownx.emoji import emojione, gemoji, twemoji
from markdown import Markdown
from markdown.extensions.footnotes import FootnoteExtension

# EXTENSIONS


# configure footnote extension
# use text ↩︎︎ character instead of emoji: https://alexwlchan.net/2017/03/extensions-in-python-markdown/
footnote_extension = FootnoteExtension(configs={
	'BACKLINK_TEXT': '&#8617;&#xFE0E;'
})



md = Markdown(extensions=['markdown.extensions.sane_lists', 'markdown.extensions.toc',
						  'markdown.extensions.codehilite', 'markdown.extensions.smarty',
						  'markdown.extensions.fenced_code', 'markdown.extensions.tables',
						  footnote_extension
						  # @formatter:off



# UNCOMMENT ADDITIONAL EXTENSIONS TO USE HERE
# make sure not to delete the leading commas

# Unenabled extensions that are included in python-markdown
#   Documentation: https://python-markdown.github.io/extensions/
# , 'markdown.extensions.abbr' # adds <abbr> tags around 'HTML', etc.
# , 'markdown.extensions.attr_list'  # allows setting HTML attributes of markdown elements
# , 'markdown.extensions.smart_strong' # conflicts with pymdownx.betterem
# , 'markdown.extensions.meta' # place metadata at top of markdown files
# , 'markdown.extensions.admonition' # allows for rST style admonitions
# , 'markdown.extensions.nl2br' # newlines are converted to <br>, don't need to leave an empty line
# , 'markdown.extensions.wikilinks' # [[bracketed]] words get converted to WikiLinks





# Other available extensions from various packages

, 'mdx_truly_sane_lists' # makes lists even more sane; pip install mdx_truly_sane_lists
#      ^^ THIS MAKES LIST INDENTS 2 SPACES, 4 SPACE LIST INDENTS WILL NOT WORK





# python-markdown extra extensions collection - pip install pymdown-extensions
#   documentation at https://facelessuser.github.io/pymdown-extensions/

# First, extensions that can conflict with other default enabled extensions:
, 'pymdownx.betterem' # improves parsing of bold and italics markers
#      conflicts with markdown.extensions.smartstrong (in markdown.extensions.extra) (see line 20)
# , 'pymdownx.extra' # shortcut for the following extensions:
#      markdown.extensions.extra, magiclink, betterem, tilde, emoji, tasklist, superfences
# , 'pymdownx.github' # github markdown simulation - pymdownx.extra plus the following:
#      markdown.extensions.tables (in markdown.extensions.extra)



# , 'pymdownx.superfences' # fences in code blocks
#      conflicts with markdown.extensions.fenced_code

, 'pymdownx.caret' # text in ^^2 carets^^ gets html <ins> and text in ^a caret^ gets superscripted
, 'pymdownx.tilde' # text in ~~2 tildes~~ gets html <del> and text in ~a tilde~ gets subscripted
, 'pymdownx.emoji' # allows easy addition of emoji
, 'pymdownx.arithmatex' # allows for usage of LaTeX equations with MathJax
#      need to uncomment the MathJax <script> tag in templates/html-head.html
# , 'pymdownx.b64' # embeds local images with base64 encoding
# , 'pymdownx.critic' # adds Critic Markup support
# , 'pymdownx.details' # adds <details><summary> tags
# , 'pymdownx.escapeall' # can escape any character with \
# , 'pymdownx.extrarawhtml' # no idea what this does
# , 'pymdownx.highlight' # allows for configuration of superfences and inlinehilite
, 'pymdownx.inlinehilite' # highlights `inline code`
#, 'pymdownx.keys' # allows using ++pluses++ instead of <kbd></kbd>, also works for shortcuts like ++ctrl+alt+delete++
#, 'pymdownx.magiclink' # automatically linkifies URLs and emails
, 'pymdownx.mark' # allows for html <mark> (highlighter) using ==2 equals signs==
# , 'pymdownx.pathconverter' # does something with link paths
# , 'pymdownx.progressbar' # allows adding progress bars with [=65% "optional title"]
, 'pymdownx.smartsymbols' # automatically symbolizes text like (c), =/=, 1/4, -->, etc.
# , 'pymdownx.snippets' # allows including other markdown/HTML files
, 'pymdownx.tasklist' # adds github-style tasklists
# , 'pymdownx.striphtml' # strips comments from compiled markdown - already done by BG2
# @formatter:on
						  ],



###   EXTENSION OPTIONS

			  extension_configs={
				  'markdown.extensions.smarty': {
					  'smart_angled_quotes': 'True'
				  },
				  'pymdownx.emoji': {
					  'emoji_index': Config.emoji_source
				  },
#				  'markdown.extensions.toc': {
#					  'title': 'Table of Contents'
#				  }
			  })

# END EXTENSIONS
