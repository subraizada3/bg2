# functions for collecting content & content metadata
from os import listdir, path
from src.code import logging
from src.code.config import Config


def get_post_filenames(content_dir: str):
	"""
	Get list of post filenames, excluding the content directory name
	:param content_dir: content directory name, without trailing slash
	"""
	return [f for f in listdir(content_dir) if
				path.isfile(content_dir + '/' + f)
				and f.endswith('.md')
				and f != 'index.md']

post_ids: dict = {} # post ID cache - dict of content dir -> list of its pids
def get_post_ids(content_dir: str): # sorted
	"""
	Get sorted list of post IDs
	:param content_dir: content directory name, without trailing slash
	"""
	global post_ids
	if content_dir not in post_ids.keys(): # init cache
		post_ids[content_dir] =\
			sorted([int(f[:-3]) for f in get_post_filenames(content_dir)])
		if Config.reverse_posts: post_ids.reverse()
	return post_ids[content_dir]

def get_posts(content_dir: str):
	"""
	Get dict of post ID (12) -> post filename ('12.md')
	:param content_dir: content directory name, without trailing slash
	"""
	result = {}
	for pid in get_post_ids(content_dir):
		result[pid] = str(pid) + '.md'
	return result


# cache dictionaries
# str (content dir) -> int (post id) -> tuple (str, str) (title, date, filename)
post_metas = {}
# str (content dir) -> int (post id) -> post contents, excluding metadata lines
posts = {}

def init(content_dir: str):
	"""
	Initialize cache of post metadata & content
	:param content_dir: content directory name, without trailing slash
	"""
	# load post metadata and contents
	global post_ids
	# first, reset caches for this content dir
	post_ids.pop(content_dir, None)
	post_metas[content_dir] = {}
	posts[content_dir] = {}
	for pid, filename in get_posts(content_dir).items():
		with open(content_dir + '/' + filename) as f:
			try:
				# extract post metadata and put it into the post_meta dict
				line = f.readline()
				date = line[:11]
				title = line[11:]
				post_metas[content_dir][pid] = (title, date, filename)
				# and put the rest of the post into the posts dict
				posts[content_dir][pid] = f.read()
			except Exception as e:
				# remove post from list of posts & post IDs
				if pid in post_ids[content_dir]:
					post_ids[content_dir].remove(pid)
				logging.trace(e,
					'Error while parsing metadata for post {}'.format(pid))
