from configparser import RawConfigParser
from pymdownx.emoji import emojione, gemoji, twemoji

class Config:

	cp = RawConfigParser()

	sitename = ''
	author = ''
	emoji_source = emojione
	webserver = True
	date_format = ''
	reverse_posts = False
	max_pid_increase = 10
	template_name = ''

	@staticmethod
	def read():
		Config.cp.read('config.ini')
		Config.sitename = Config.cp['cfg']['SiteName']
		Config.author = Config.cp['cfg']['Author']
		Config.webserver = Config.cp['cfg']['Webserver'][0] == '1'
		Config.date_format = Config.cp['cfg']['DateFormat']
		Config.reverse_posts = Config.cp['cfg']['ReverseAllPosts'][0] == '1'
		Config.max_pid_increase = int(Config.cp['cfg']['MaxPostIDIncrease'])
		Config.template_name = Config.cp['cfg']['TemplateName']

		if Config.cp['cfg']['EmojiSource'] == 'emojione':
			Config.emoji_source = emojione
		elif Config.cp['cfg']['EmojiSource'] == 'gemoji':
			Config.emoji_source = gemoji
		elif Config.cp['cfg']['EmojiSource'] == 'twemoji':
			Config.emoji_source = twemoji
