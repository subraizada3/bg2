# functions for templating a post or page

import re
import sys
from datetime import date, datetime

from src.code import logging
from src.code.config import Config

post_template = ''
homepage_template = ''

template_content_dir = '' # 'staging_templates' or 'live_templates'
template_name = ''        # defaults are 'light' or 'sepia'
template_dir = ''         # 'staging_templates/light'

current_year = str(datetime.now().year)

# methods for some basic minification
# remove empty lines happens before processing
#   otherwise empty lines in code blocks would be removed
# remove comments can safely happen at the end
def remove_empty_lines(html):
	return "\n".join([s for s in html.splitlines() if s])

def remove_html_comments(html):
	# remove comments - https://stackoverflow.com/a/28208465
	# yes, this uses regex on HTML
	return re.sub("<!--.*?-->", "", html, flags=re.DOTALL)


def init(template_content_dir_local, template_name_local):
	"""
	Init the caches for this template
	:param template_content_dir_local: name of templates base directory to use
	:param template_name_local: name of template to use
	"""
	global template_content_dir, template_name, template_dir
	template_content_dir = template_content_dir_local
	template_name = template_name_local
	template_dir = template_content_dir + '/' + template_name

	global post_template, homepage_template
	try:
		post_template = open(template_dir + '/post.html').read()
		post_template = remove_empty_lines(post_template)
		homepage_template = open(template_dir + '/index.html').read()
		homepage_template = remove_empty_lines(homepage_template)
	except FileNotFoundError:
		logging.error(
			'post.html and index.html not found in {}'.format(template_dir))
		sys.exit(0)


# cache to prevent reading included files every time
# double dict, from template name -> include filename -> include content
include_cache = {}

# replace {{include filename}} with the file's contents
def complete_includes(html):
	if template_name not in include_cache: include_cache[template_name] = {}

	index = html.find('{{include ')
	while index >= 0:
		remainder = html[index+10:] # get the chars after {{include<space>
		include_name = remainder.split("}}")[0] # ... but before the closing }}

		# either load the file to include from the cache, or read & cache it
		if include_name in include_cache[template_name]:
			include_replace = include_cache[template_name][include_name]
		else:
			try:
				include_replace =\
					open(template_dir + '/include/' + include_name).read()
				if include_name.endswith('.html')\
					or include_name.endswith('.css')\
					or include_name.endswith('.js'):
					include_replace = remove_empty_lines(include_replace)
			except FileNotFoundError:
				logging.error('{} not found in {} - check your config.ini'
					  .format(include_name, template_dir + '/include/'))
				continue
			include_cache[template_name][include_name] = include_replace

		html = html.replace("{{include " + include_name + "}}", include_replace)
		index = html.find("{{include ")

	return html


# perform templating common to posts and pages
#   body content, post/page title, sitename, author, current year, and includes
def common(string, mdhtml):
	# order: insert body, complete includes, do everything else
	tmp = complete_includes(string.replace('{{body}}', mdhtml))

	# use regex to replace everything else in one pass
	# from https://stackoverflow.com/a/6117124 and the github gist in comments
	replace_dict = {
		'{{sitename}}': Config.sitename,
		'{{year}}': current_year,
		'{{author}}': Config.author
	}
	substrs = sorted(replace_dict, key=len, reverse=True)
	regex = re.compile('|'.join(map(re.escape, substrs)))
	return regex.sub(lambda match: replace_dict[match.group(0)], tmp)


# templating for posts: common + post date + links to next/prev post
def post(mdhtml, title, date_str,
		 prev_title, prev_url, next_title, next_url):
	post_template_copy = post_template

	# get the formatted date form the post's meta date (in 2018-12-31 format)
	datearray = date_str.split('-')
	post_date = date(int(datearray[0]), int(datearray[1]), int(datearray[2]))

	tmp = common(post_template_copy, mdhtml)

	# use regex to do all the replacements in one pass
	replace_dict = {
		'{{title}}': title,
		'{{date}}': post_date.strftime(Config.date_format),
		'{{prev-title}}': prev_title,
		'{{prev-url}}': prev_url,
		'{{next-title}}': next_title,
		'{{next-url}}': next_url,
	}
	substrs = sorted(replace_dict, key=len, reverse=True)
	regex = re.compile('|'.join(map(re.escape, substrs)))
	tmp= regex.sub(lambda match: replace_dict[match.group(0)], tmp)

	return remove_html_comments(tmp)


# noinspection PyIncorrectDocstring
def all_posts_list(html: str, posts: dict):
	"""
	Fill in the list of all posts (on the homepage)
	:param posts: dict of pid -> (title, _, filename) (content.post_metas style)
	"""
	try:
		list_template = html.split('{{begin-list posts}}')[1]\
			.split('{{end-list posts}}')[0]
	except IndexError: # list isn't present in this file
		return html

	# assemble the list into this, and then replace {{place-list}} with this
	replace_str = ''

	pids = sorted(posts.keys())
	if Config.reverse_posts: pids = reversed(pids)
	for pid in [x for x in pids if x > 0]:
		this_item = list_template
		this_item = this_item.replace('{{list-element title}}', posts[pid][0])\
			.replace('{{list-element url}}', posts[pid][2][:-3] + '.html')
		replace_str += this_item
	return html.replace('{{place-list posts}}', replace_str)


# templating for homepage: common + all posts list
def homepage(mdhtml, all_posts):
	homepage_template_copy = homepage_template
	tmp = common(homepage_template_copy, mdhtml)

	# all posts list
	tmp = all_posts_list(tmp, all_posts)

	return remove_html_comments(tmp)
