import os, shutil, glob

# remove files in live and live_templates
files = glob.glob('live/*') + glob.glob('live/.*')\
        + glob.glob('live_templates/*') + glob.glob('live_templates/.*')
for f in files:
	if os.path.isfile(f): os.remove(f)
	else: shutil.rmtree(f)

# copy staging to live
for f in os.listdir('staging'):
	if os.path.isfile('staging/' + f):
		shutil.copy('staging/' + f, 'live/' + f)
	else:
		shutil.copytree('staging/' + f, 'live/' + f)

# copy staging_templates to live_templates
for f in os.listdir('staging_templates'):
	if os.path.isfile('staging_templates/' + f):
		shutil.copy('staging_templates/' + f, 'live/' + f)
	else:
		shutil.copytree('staging_templates/' + f, 'live_templates/' + f)
