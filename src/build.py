# build posts and homepage and place them into the output folder

import os, glob, shutil
from src.code import content, template, logging
from src.code.config import Config

Config.read()
# load config before importing this
from src.code import md_builder
md = md_builder.md

# clean output directory, or make it if it doesn't exist
def clean_or_make_output_dir(dir: str):
	"""
	Clean output directory, or make it if it doesn't exist
	:param dir: output directory name, without trailing slash
	"""
	if not os.path.exists(dir):
		os.makedirs(dir)
	else:
		# need to delete the directory contents without deleting the directory
		# otherwise webserver will keep trying to serve the old folder & fail
		# https://stackoverflow.com/a/5756937
		files = glob.glob(dir + '/*') + glob.glob(dir + '/.*')
		try:
			for f in files:
				if os.path.isfile(f):
					os.remove(f)
				else:
					shutil.rmtree(f)
		except Exception as e:
			logging.trace(e, 'Error while clearing output directory ' + dir)


# copy all non-HTML files from templates/ to out/
def copy_templates_to_output_dir(output_dir: str, template_dir: str):
	"""
	Copy all non-HTML files in the template and all folders in the template (except for 'include') to the output directory
	:param output_dir: output directory name, without trailing slash
	:param template_dir: full path of template directory, without trailing slash
	"""
	for f in os.listdir(template_dir):
		try:
			if os.path.isfile(template_dir + '/' + f) and not f.endswith('.html'):
				shutil.copy(template_dir + '/' + f, output_dir + '/' + f)
			elif os.path.isdir(template_dir + '/' + f) and f != 'include':
				shutil.copytree(template_dir + '/' + f, output_dir + '/' + f)
		except Exception as e:
			logging.trace(e, 'Error while copying {} to {}'
				  .format(template_dir + '/' + f, output_dir + '/' + f))


def copy_non_markdown_content_to_output_dir(content_dir: str, output_dir: str):
	"""
	Copy all non-markdown files from content dir to output dir
	:param content_dir: content directory name, without trailing slash
	:param output_dir: output directory name, without trailing slash
	"""
	for f in [x for x in os.listdir(content_dir) if
			  os.path.isfile(content_dir + '/' + x) and not x.endswith('.md')]:
		try:
			shutil.copy(content_dir + '/' + f, output_dir + '/' + f)
		except Exception as e:
			logging.trace(e, 'Error while copying content {} to {}'
				  .format(content_dir + '/' + f, output_dir + '/' + f))


# process posts
def process_posts(content_dir: str, output_dir: str):
	pids = content.get_post_ids(content_dir)
	for pid in pids:
		try:
			# need to reset the footnote extension between building pages
			md_builder.footnote_extension.reset()
			mdhtml = md.convert(content.posts[content_dir][pid])

			# find the title/url of next/previous post
			i = pids.index(pid)
			ppid = -1 if i == 0 else pids[i-1] # previous pid
			npid = -1 if i == len(pids) - 1 else pids[i+1] # next pid

			# if no previous post, or previous post is hidden, or this post is
			# hidden, don't make a list to the previous post
			if ppid < 0 or pid < 0:
				prev_title = ''; prev_url = ''
			else:
				prev_title = content.post_metas[content_dir][ppid][0]
				prev_url = content.post_metas[content_dir][ppid][2][:-3]+ '.html'

			# if no next post or this post is hidden dont make link to next post
			if npid == -1 or pid < 0:
				next_title = ''; next_url = ''
			else:
				next_title = content.post_metas[content_dir][npid][0]
				next_url = content.post_metas[content_dir][npid][2][:-3] + '.html'

			# template the post with the mdhtml, title, and date
			with open(output_dir + '/' + str(pid) + '.html', 'w') as f:
				f.write(template.post(mdhtml,
									  content.post_metas[content_dir][pid][0],
									  content.post_metas[content_dir][pid][1],
									  prev_title, prev_url, next_title, next_url))
		except Exception as e:
			logging.trace(e, 'Error while building post {}'.format(pid))


# process homepage
def process_homepage(content_dir: str, output_dir: str):
	try: homepage = open(content_dir + '/index.md')
	except FileNotFoundError:
		logging.error(
			'index.md not found in {}/ - not building homepage'
				.format(content_dir))
		return

	try:
		md_builder.footnote_extension.reset()
		homepage_mdhtml = md.convert(homepage.read())
		with open(output_dir + '/index.html', 'w') as f:
			f.write(template.homepage(homepage_mdhtml,
									  content.post_metas[content_dir]))
	except Exception as e:
		logging.trace(e, 'Error while building homepage - skipping')


def build(content_dir: str, output_dir: str, template_name: str):
	content.init(content_dir) # initialize post metadata/content cache
	template.init(content_dir + '_templates', template_name) # init cache
	clean_or_make_output_dir(output_dir)
	copy_templates_to_output_dir(output_dir,
								 content_dir + '_templates/' + template_name)
	copy_non_markdown_content_to_output_dir(content_dir, output_dir)
	process_posts(content_dir, output_dir)
	process_homepage(content_dir, output_dir)
