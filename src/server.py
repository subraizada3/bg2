# serve the staging_out/ folder on localhost:8106

# move to the 'out' directory
# the main bg2 script already moves us to the blog root directory
import os; os.chdir('staging_out')

# start the server
import http.server, socketserver

handler = http.server.SimpleHTTPRequestHandler

# override the HTTPRequestHandler's log_message function to suppress output
# noinspection PyUnusedLocal,PyShadowingBuiltins
def do_nothing(self, format, *args): pass
handler.log_message = do_nothing
handler.log_error   = do_nothing

# this works around needing to wait about a minute to successfully restart the
#   server after killing a previous instance of the server
socketserver.TCPServer.allow_reuse_address = True

with socketserver.TCPServer(('localhost', 8106), handler) as httpd:
	httpd.serve_forever()
