import multiprocessing
from src.code import logging
from src.code.config import Config


def server(): exec(open('src/server.py').read())

def daemon():
	import inotify.adapters, threading
	from src.code.config import Config

	# locks to prevent concurrent building
	locks = { 'staging': threading.Lock(),
			  'live':    threading.Lock(),
			  'config':  threading.Lock()  }

	# can't use the with-style locks, because those must be blocking
	def build(sleep_time, content_dir, output_dir):
		if not locks[content_dir].acquire(blocking=False): return
		try:
			# this method triggers on the first inotify event
			# delay to wait for all file changes to happen before we start building
			# 1 second for staging (time to finish writing an edited file)
			# 5 seconds for live (time for all files to copy from staging to live)
			import time; time.sleep(sleep_time)
			from src.build import build
			build(content_dir, output_dir, Config.template_name)
		finally:
			locks[content_dir].release()


	def reload_config():
		if not locks['config'].acquire(blocking=False): return
		try:
			import time; time.sleep(0.5)
			Config.read()
		finally:
			locks['config'].release()


	# build either 'staging' or 'live' in a separate thread after a delay
	def start_build_thread(target, delay): # delay in seconds
		threading.Thread(target=build,
						 args=[delay, target, target + '_out']).start()


	# act only on write events, ignore accessed events
	def is_relevant_event(event_1):
		return 'IN_CLOSE_WRITE'    in event_1\
				or 'IN_CREATE'     in event_1\
				or 'IN_DELETE'     in event_1\
				or 'IN_MODIFY'     in event_1\
				or 'IN_MOVED_TO'   in event_1\
				or 'IN_MOVED_FROM' in event_1


	# templates: the user can change the active template via the config file
	# we watch the entire program base dir, because we can't put a watch on
	#   just the config file
	# this also removes the need to have a separate non-recursive inotify watch
	#   for posts
	# we watch for changes to config.ini, the active template, and content
	j = inotify.adapters.InotifyTrees(['.'])
	for event in j.event_gen():
		if event is not None and is_relevant_event(event[1]):

			event_dir = event[2]
			event_file = event[3]

			# config file changed - reload config
			if event_dir == '.' and event_file == 'config.ini':
				threading.Thread(target=reload_config).start()
				start_build_thread('staging', 1)

			# staging content changed - rebuild
			elif event_dir == './staging':
				start_build_thread('staging', 1)

			# live content changed - rebuild after 5 seconds
			elif event_dir == './live':
				start_build_thread('live', 5)

			# staging templates changed - rebuild if active template changed
			elif event_dir.startswith('./staging_templates'):
				event_dir_parts = event_dir.split('/')
				if len(event_dir_parts) < 3: continue
				event_template_name = event_dir_parts[2]

				if event_template_name == Config.template_name:
					start_build_thread('staging', 1)

			# live templates changed - rebuild if active template changed
			elif event_dir.startswith('./live_templates'):
				event_dir_parts = event_dir.split('/')
				if len(event_dir_parts) < 3: continue
				event_template_name = event_dir_parts[2]

				if event_template_name == Config.template_name:
					start_build_thread('live', 5)

multiprocessing.Process(target=daemon).start()

try:
	Config.read()
except Exception as e:
	logging.trace(e, 'Failed to parse configuration file')

if Config.webserver:
	multiprocessing.Process(target=server).start()
