# make a new post with random ID, in the staging folder

import random
import sys
from datetime import datetime

from src.code.config import Config
from src.code.content import get_post_ids

# collect existing post IDs
pids = get_post_ids('staging')
max_id = max(pids) if len(pids) > 0 else 0

# make an ID for the new post, within 20 of the previous max post ID
pid = random.randint(max_id + 1, max_id + Config.max_pid_increase)

# get post information from user and write out the post stub
title = input('Post title: ')
if title == '':
	print('Invalid title')
	sys.exit(0)

if input("Enter 'h' to make post hidden: ").lower() == 'h':
	pid *= -1

date = datetime.today().strftime('%Y-%m-%d')

f = open('staging/' + str(pid) + '.md', 'w')
f.write(date + ' ' + title + '\n')
f.close()

print('Created file staging/' + str(pid) + '.md')
