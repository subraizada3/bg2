CODEHILITE STYLES

by Rich Leland - https://github.com/richleland/pygments-css

The default themes include the default CodeHilite CSS (default.css).
To use another style, copy it from here into templates/.../codehilite.css

Demo of styles: http://richleland.github.io/pygments-css/
